#!/bin/bash
cd src
mkdir -p ../bin
echo "Compiling controller.c"
gcc -c controller.c -lX11
echo "Compiling display.c"
gcc -c display.c -lX11
echo "Compiling events.c"
gcc -c events.c -lX!!
echo "Compiling functions.c"
gcc -c functions.c
echo "Compiling timer.c"
gcc -c timer/timer.c
echo "Linking"
gcc controller.o display.o events.o functions.o timer.o \
	-o ../bin/controller -lX11 -D Client
cd ../
sloccount ../brewerycontroller/
