#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h> // Every Xlib program must include this
#include "display.ext"
#include "functions.ext"

unsigned int Process_Selector_Window_Events(void);

unsigned int Process_Selector_Window_Events(void)
{
	unsigned char result = 0;
	XEvent event;
    Window w;
	Display_Selector_Window();
	while (!OKToExit)
	{
		if (XPending(dsp))
		{
			XNextEvent(dsp,&event);
			switch (event.type)
			{
				case KeyPress :
					if (event.xkey.keycode == 9)			//Escape Key
						OKToExit = 1;
					break;
				case ButtonPress :
					w = event.xbutton.window;
					if (event.xbutton.button == Button1)
					{
						if (w == SelectorWindowLoginButton)
						{
							Process_Selector_Window_Login_Button();
						}
					}
			}
		}
	}
}
