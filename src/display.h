#ifndef Display_H
#define Display_H

#define ButtonPressDelayTime                    100

Display *dsp;
Window MainWindow;
GC gcMainWindow;//MainGC;

int s;
XFontStruct *font;
XCharStruct TextBounds;
XGCValues values;
unsigned long valuemask;
int dummy;


void Initialize_Display(void);
void Display_Selector_Window(void);
void Display_Login_Window(void);

#endif /*Display_H*/
