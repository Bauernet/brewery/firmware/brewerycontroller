#ifndef Display_Colors_h
#define Display_Colors_h

//Colors are defined in RGB format, these values need to be converted to decimal for use here

#define ColorBlack                          0           //#000000
#define ColorBlue                           256         //#0000FF
//#define ColorGreen                          65280       //#00FF00
#define ColorGreen                          1232402
#define ColorRed                            16711680    //#FF0000
#define ColorWhite                          16777215    //#FFFFFF

#define BackgroundColor                     12632256    //#C0C0C0  //Light Grey
//#define BackgroundColor                     1577678     //#1812CE //Blue

#define ButtonBorderColorA                  14737632       //#E0E0E0
#define ButtonBorderColorB                  0                //#000000
#define ButtonBackgroundColor               10526880      //#A0A0A0
#define ButtonTextColor                     16777215            //#FFFFFF

#define TextBoxColor                        0                      //#000000
#define TextBoxBackgroundColor              10526880     //#A0A0A0
#define TextBoxBorderColorA                 0                //#000000
#define TextBoxBorderColorB                 14737632         //#E0E0E0

#endif //Display_Colors_h
