#ifndef Display_Coords_h
#define Display_Coords_h

//This file contains the co-ordinates and sizes of all windows, buttons, boxes etc

//Main Display
#define MainWindowXLoc                                          0
#define MainWindowYLoc                                          0
#define MainWindowWidth                                         1024
#define MainWindowHeight                                        600

/*Our main selector window*/
Window SelectorWindow = (long unsigned int)NULL;
GC gcSelectorWindow = NULL;

#define SelectorWindowXLoc											0
#define SelectorWindowYLoc											500
#define SelectorWindowWidth											1024
#define SelectorWindowHeight										100
	Window SelectorWindowLoginButton = (long unsigned int)NULL;
	GC gcSelectorWindowLoginButton = NULL;
	#define SelectorWindowLoginButtonXLoc							2
	#define SelectorWindowLoginButtonYLoc							2
	#define SelectorWindowLoginButtonWidth							100
	#define SelectorWindowLoginButtonHeight							47

/*Our first window - Login*/
Window LoginWindow = (long unsigned int)NULL;
GC gcLoginWindow = NULL;

#define LoginWindowXLoc												0
#define LoginWindowYLoc												0
#define LoginWindowWidth											1024
#define LoginWindowHeight											600
	#define LoginWindowUserTectBoxXLoc								100
	#define LoginWindowUserTextBoxYLoc								100
	#define LoginWindowUserTextBoxWitdh								924
	#define LoginWindowUserTextBoxHeight							100

	#define LoginWindowPassTextBoxXLoc								100
	#define LoginWindowPassTextBoxYLoc								300
	#define LoginWindowPassTextBoxWitdh								924
	#define LoginWindowPassTextBoxHeight							100

/*An AlphaNumeric Keyboard*/
Window AlphaNumWindow = (long unsigned int)NULL;
GC gcAlhpaNumWindow = NULL;

#define AlphaNumWindowXLoc                                         0
#define AlphaNumWindowYLoc                                         0
#define AlphaNumWindowWidth                                        640
#define AlphaNumWindowHeight                                       420
    //Buttons
    #define AlphaNumWindowButtonWidth                                    36
    #define AlphaNumWindowButtonHeight                                   36
	Window AlphaNumWindowAButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowAButtonWindow = NULL;
	#define AlphaNumWindowAButtonXLoc                                    40
    #define AlphaNumWindowAButtonYLoc                                    262
	Window AlphaNumWindowBButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowBButtonWindow = NULL;
    #define AlphaNumWindowBButtonXLoc                                    222
    #define AlphaNumWindowBButtonYLoc                                    302
	Window AlphaNumWindowCButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowCButtonWindow = NULL;
    #define AlphaNumWindowCButtonXLoc                                    142
    #define AlphaNumWindowCButtonYLoc                                    302
	Window AlphaNumWindowDButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowDButtonWindow = NULL;
    #define AlphaNumWindowDButtonXLoc                                    120
    #define AlphaNumWindowDButtonYLoc                                    262
	Window AlphaNumWindowEButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowEButtonWindow = NULL;
    #define AlphaNumWindowEButtonXLoc                                    102
    #define AlphaNumWindowEButtonYLoc                                    222
	Window AlphaNumWindowFButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowFButtonWindow = NULL;
    #define AlphaNumWindowFButtonXLoc                                    160
    #define AlphaNumWindowFButtonYLoc                                    262
	Window AlphaNumWindowGButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowGButtonWindow = NULL;
	#define AlphaNumWindowGButtonXLoc                                    200
    #define AlphaNumWindowGButtonYLoc                                    262
	Window AlphaNumWindowHButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowHButtonWindow = NULL;
    #define AlphaNumWindowHButtonXLoc                                    240
    #define AlphaNumWindowHButtonYLoc                                    262
	Window AlphaNumWindowIButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowIButtonWindow = NULL;
	#define AlphaNumWindowIButtonXLoc                                    302
    #define AlphaNumWindowIButtonYLoc                                    222
	Window AlphaNumWindowJButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowJButtonWindow = NULL;
    #define AlphaNumWindowJButtonXLoc                                    280
    #define AlphaNumWindowJButtonYLoc                                    262
	Window AlphaNumWindowKButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowKButtonWindow = NULL;
    #define AlphaNumWindowKButtonXLoc                                    320
    #define AlphaNumWindowKButtonYLoc                                    262
	Window AlphaNumWindowLButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowLButtonWindow = NULL;
    #define AlphaNumWindowLButtonXLoc                                    360
    #define AlphaNumWindowLButtonYLoc                                    262
	Window AlphaNumWindowMButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowMButtonWindow = NULL;
    #define AlphaNumWindowMButtonXLoc                                    302
    #define AlphaNumWindowMButtonYLoc                                    302
	Window AlphaNumWindowNButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowNButtonWindow = NULL;
    #define AlphaNumWindowNButtonXLoc                                    262
    #define AlphaNumWindowNButtonYLoc                                    302
	Window AlphaNumWindowOButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowOButtonWindow = NULL;
    #define AlphaNumWindowOButtonXLoc                                    342
    #define AlphaNumWindowOButtonYLoc                                    222
	Window AlphaNumWindowPButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowPButtonWindow = NULL;
    #define AlphaNumWindowPButtonXLoc                                    382
    #define AlphaNumWindowPButtonYLoc                                    222
	Window AlphaNumWindowQButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowQButtonWindow = NULL;
	#define AlphaNumWindowQButtonXLoc                                    22
    #define AlphaNumWindowQButtonYLoc                                    222
	Window AlphaNumWindowRButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowRButtonWindow = NULL;
    #define AlphaNumWindowRButtonXLoc                                    142
    #define AlphaNumWindowRButtonYLoc                                    222
	Window AlphaNumWindowSButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowSButtonWindow = NULL;
    #define AlphaNumWindowSButtonXLoc                                    80
    #define AlphaNumWindowSButtonYLoc                                    262
	Window AlphaNumWindowTButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowTButtonWindow = NULL;
    #define AlphaNumWindowTButtonXLoc                                    182
    #define AlphaNumWindowTButtonYLoc                                    222
	Window AlphaNumWindowUButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowUButtonWindow = NULL;
    #define AlphaNumWindowUButtonXLoc                                    262
    #define AlphaNumWindowUButtonYLoc                                    222
	Window AlphaNumWindowVButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowVButtonWindow = NULL;
    #define AlphaNumWindowVButtonXLoc                                    182
    #define AlphaNumWindowVButtonYLoc                                    302
	Window AlphaNumWindowWButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowWButtonWindow = NULL;
    #define AlphaNumWindowWButtonXLoc                                    62
    #define AlphaNumWindowWButtonYLoc                                    222
	Window AlphaNumWindowXButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowXButtonWindow = NULL;
    #define AlphaNumWindowXButtonXLoc                                    102
    #define AlphaNumWindowXButtonYLoc                                    302
	Window AlphaNumWindowYButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowYButtonWindow = NULL;
    #define AlphaNumWindowYButtonXLoc                                    222
    #define AlphaNumWindowYButtonYLoc                                    222
	Window AlphaNumWindowZButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowZButtonWindow = NULL;
    #define AlphaNumWindowZButtonXLoc                                    62
    #define AlphaNumWindowZButtonYLoc                                    302
	Window AlphaNumWindow0ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow0ButtonWindow = NULL;
    #define AlphaNumWindow0ButtonXLoc                                    542
    #define AlphaNumWindow0ButtonYLoc                                    342
	Window AlphaNumWindow1ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow1ButtonWindow = NULL;
    #define AlphaNumWindow1ButtonXLoc                                    502
    #define AlphaNumWindow1ButtonYLoc                                    302
	Window AlphaNumWindow2ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow2ButtonWindow = NULL;
    #define AlphaNumWindow2ButtonXLoc                                    542
    #define AlphaNumWindow2ButtonYLoc                                    302
	Window AlphaNumWindow3ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow3ButtonWindow = NULL;
    #define AlphaNumWindow3ButtonXLoc                                    582
    #define AlphaNumWindow3ButtonYLoc                                    302
	Window AlphaNumWindow4ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow4ButtonWindow = NULL;
    #define AlphaNumWindow4ButtonXLoc                                    502
    #define AlphaNumWindow4ButtonYLoc                                    262
	Window AlphaNumWindow5ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow5ButtonWindow = NULL;
    #define AlphaNumWindow5ButtonXLoc                                    542
    #define AlphaNumWindow5ButtonYLoc                                    262
	Window AlphaNumWindow6ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow6ButtonWindow = NULL;
    #define AlphaNumWindow6ButtonXLoc                                    582
    #define AlphaNumWindow6ButtonYLoc                                    262
	Window AlphaNumWindow7ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow7ButtonWindow = NULL;
    #define AlphaNumWindow7ButtonXLoc                                    502
    #define AlphaNumWindow7ButtonYLoc                                    222
	Window AlphaNumWindow8ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow8ButtonWindow = NULL;
    #define AlphaNumWindow8ButtonXLoc                                    542
    #define AlphaNumWindow8ButtonYLoc                                    222
	Window AlphaNumWindow9ButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindow9ButtonWindow = NULL;
    #define AlphaNumWindow9ButtonXLoc                                    582
    #define AlphaNumWindow9ButtonYLoc                                    222
	Window AlphaNumWindowDelButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowDelButtonWindow = NULL;
    #define AlphaNumWindowDelButtonXLoc                                  422
    #define AlphaNumWindowDelButtonYLoc                                  222
    #define AlphaNumWindowDelButtonWidth                                 76
    #define AlphaNumWindowDelButtonHeight                                36
	Window AlphaNumWindowSpaceButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowSpaceButtonWindow = NULL;
    #define AlphaNumWindowSpaceButtonXLoc                                142
    #define AlphaNumWindowSpaceButtonYLoc                                342
    #define AlphaNumWindowSpaceButtonWidth                               196
    #define AlphaNumWindowSpaceButtonHeight                              36
	Window AlphaNumWindowEnterButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowEnterButtonWindow = NULL;
    #define AlphaNumWindowEnterButtonXLoc                                342
    #define AlphaNumWindowEnterButtonYLoc                                302
    #define AlphaNumWindowEnterButtonWidth                               116
    #define AlphaNumWindowEnterButtonHeight                              36
	Window AlphaNumWindowCapsButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowCapsButtonWindow = NULL;
    #define AlphaNumWindowCapsButtonXLoc                                 22
    #define AlphaNumWindowCapsButtonYLoc                                 302
    #define AlphaNumWindowCapsButtonWidth                                36
    #define AlphaNumWindowCapsButtonHeight                               36
	Window AlphaNumWindowPlusButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowPlusButtonWindow = NULL;
    #define AlphaNumWindowPlusButtonXLoc                                 462
    #define AlphaNumWindowPlusButtonYLoc                                 262
	Window AlphaNumWindowMinusButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowMinusButtonWindow = NULL;
    #define AlphaNumWindowMinusButtonXLoc                                462
    #define AlphaNumWindowMinusButtonYLoc                                302
	Window AlphaNumWindowDecButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowDecButtonWindow = NULL;
    #define AlphaNumWindowDecButtonXLoc                                  582
    #define AlphaNumWindowDecButtonYLoc                                  342
	Window AlphaNumWindowCancelButtonWindow = (long unsigned int)NULL;
	GC gcAlphaNumWindowCancelButtonWindow = NULL;
    #define AlphaNumWindowCancelButtonXLoc                               342
    #define AlphaNumWindowCancelButtonYLoc                               342
    #define AlphaNumWindowCancelButtonWidth                              116
    #define AlphaNumWindowCancelButtonHeight                             36

	Window AlphaNumWindowInputTextBox = (long unsigned int)NULL;
	GC gcAlphaNumWindowInputTextBox = NULL;
    #define AlphaNumWindowInputTextBoxXLoc                               20
    #define AlphaNumWindowInputTextBoxYLoc                               182
    #define AlphaNumWindowInputTextBoxWidth                              600
    #define AlphaNumWindowInputTextBoxHeight                             36

	Window AlphaNumWindowInputTitleTextBox = (long unsigned int)NULL;
	GC gcAlphaNumWindowInputTitleTextBox = NULL;
	#define AlphaNumWindowInputTitleTextBoxXLoc                          20
    #define AlphaNumWindowInputTitleTextBoxYLoc                          142
    #define AlphaNumWindowInputTitleTextBoxWidth                         600
    #define AlphaNumWindowInputTitleTextBoxHeight                        36




#endif /*Display_Coords_h*/
