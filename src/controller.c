/*Dependencies

X11 - sudo apt install libx11-dev

*/

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h> // Every Xlib program must include this
#include <time.h>
#include <string.h>

#include "display.ext"
#include "events.h"
#include "functions.ext"
#include "timer/timer.h"

unsigned int OneSecondTimer = 100;

void Timer_Handler(void);
void ForceTimerStop(void);
void ReStartTimer(void);

int main(void)
{
	Initialize_Display();
    ReStartTimer();
    XFlush(dsp);
    OKToExit = 0;

    Process_Selector_Window_Events();

    ForceTimerStop();
	return 0;
}

void ForceTimerStop(void)
{
    stop_timer();
}

void ReStartTimer(void)
{
    start_timer(10,&Timer_Handler);
}

void Timer_Handler(void)
{
    if (DelayTime) DelayTime--;
    if (!OneSecondTimer)
    {
        OneSecondTimer = 100;
    }
}

