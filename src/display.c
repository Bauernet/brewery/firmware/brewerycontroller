#include <X11/Xlib.h> // Every Xlib program must include this
#include <stdio.h>
#include <string.h>
#include "display_coords.h"
#include "display_colors.h"
#include "display.h"
#include "functions.ext"

void Display_Selector_Window_Login_Button(void);
void Process_Selector_Window_Login_Button(void);

void Initialize_Display(void)
{
    dsp = XOpenDisplay(NULL);
    MainWindow = XCreateSimpleWindow(dsp, DefaultRootWindow(dsp),MainWindowXLoc,MainWindowYLoc,MainWindowWidth,MainWindowHeight, 0, ColorBlack, BackgroundColor);
#ifdef RPI_BUILD
    //Set full screen mode here...
#endif // RPI_BUILD
    XSelectInput(dsp, MainWindow, StructureNotifyMask | KeyPressMask);  //Structure Mask ??? | KeyPressMask (Keyboard Actions)
    XMapWindow(dsp, MainWindow);
    gcMainWindow = XCreateGC(dsp, MainWindow, 0, NULL);
    XSetForeground(dsp, gcMainWindow, ColorWhite);
    for(;;)
    {
        XEvent e;
	    XNextEvent(dsp, &e);
	    if (e.type == MapNotify)
            break;
    }
}

void Display_Selector_Window(void)
{
    if (!SelectorWindow)
    {
        font = XLoadQueryFont(dsp, "12x24");
        s = DefaultScreen(dsp);
        values.foreground = XBlackPixel(dsp, s);
        values.font = font->fid;
        valuemask = 0;
        valuemask |= GCForeground | GCFont;
        SelectorWindow = XCreateSimpleWindow(dsp, MainWindow,SelectorWindowXLoc,SelectorWindowYLoc,SelectorWindowWidth,SelectorWindowHeight,0,ColorWhite,BackgroundColor);
        XSelectInput(dsp, SelectorWindow, StructureNotifyMask);
        XMapWindow(dsp, SelectorWindow);
        gcSelectorWindow = XCreateGC(dsp, SelectorWindow, 0, NULL);
    }
    XRaiseWindow(dsp,SelectorWindow);
    XClearWindow(dsp,SelectorWindow);
    XSetForeground(dsp,gcSelectorWindow,ButtonBorderColorA);
    XDrawLine(dsp,SelectorWindow,gcSelectorWindow,0,0,SelectorWindowWidth-1,0);
    XDrawLine(dsp,SelectorWindow,gcSelectorWindow,0,0,0,SelectorWindowHeight-1);
    XSetForeground(dsp,gcSelectorWindow,ButtonBorderColorB);
    XDrawLine(dsp,SelectorWindow,gcSelectorWindow,0,SelectorWindowHeight-1,SelectorWindowWidth-1,SelectorWindowHeight-1);
    XDrawLine(dsp,SelectorWindow,gcSelectorWindow,SelectorWindowWidth-1,0,SelectorWindowWidth-1,SelectorWindowHeight-1);
    XSetForeground(dsp,gcSelectorWindow,TextBoxColor);

    Display_Selector_Window_Login_Button();

    XFlush(dsp);
}

void Display_Selector_Window_Login_Button(void)
{
    if (!SelectorWindowLoginButton)
    {
    	SelectorWindowLoginButton = XCreateSimpleWindow(dsp,SelectorWindow,SelectorWindowLoginButtonXLoc,SelectorWindowLoginButtonYLoc,SelectorWindowLoginButtonWidth,SelectorWindowLoginButtonHeight,0,ButtonTextColor,ButtonBackgroundColor);
        XSelectInput(dsp,SelectorWindowLoginButton, StructureNotifyMask|ButtonPressMask);
        XMapWindow(dsp,SelectorWindowLoginButton);
        gcSelectorWindowLoginButton = XCreateGC(dsp,SelectorWindowLoginButton,valuemask,&values);
    }
    XSetForeground(dsp,gcSelectorWindowLoginButton,ButtonBorderColorA);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,0,SelectorWindowLoginButtonWidth-1,0);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,0,0,SelectorWindowLoginButtonHeight-1);
    XSetForeground(dsp,gcSelectorWindowLoginButton,ButtonBorderColorB);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,SelectorWindowLoginButtonHeight-1,SelectorWindowLoginButtonWidth-1,SelectorWindowLoginButtonHeight-1);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,SelectorWindowLoginButtonWidth-1,0,SelectorWindowLoginButtonWidth-1,SelectorWindowLoginButtonHeight-1);

    XTextExtents(font,"Login",sizeof("Login"),&dummy,&dummy,&dummy,&TextBounds);
    XDrawString(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,(SelectorWindowLoginButtonWidth-TextBounds.width)/2,SelectorWindowLoginButtonHeight-((SelectorWindowLoginButtonHeight-TextBounds.ascent)/2),"Login",sizeof("Login"));

    XFlush(dsp);
}

void Process_Selector_Window_Login_Button(void)
{
    XSetForeground(dsp,gcSelectorWindowLoginButton,ButtonBorderColorB);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,0,SelectorWindowLoginButtonWidth-1,0);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,0,0,SelectorWindowLoginButtonHeight-1);
    XSetForeground(dsp,gcSelectorWindowLoginButton,ButtonBorderColorA);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,0,SelectorWindowLoginButtonHeight-1,SelectorWindowLoginButtonWidth-1,SelectorWindowLoginButtonHeight-1);
    XDrawLine(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,SelectorWindowLoginButtonWidth-1,0,SelectorWindowLoginButtonWidth-1,SelectorWindowLoginButtonHeight-1);
    XTextExtents(font,"Login",sizeof("Login"),&dummy,&dummy,&dummy,&TextBounds);
    XDrawString(dsp,SelectorWindowLoginButton,gcSelectorWindowLoginButton,(SelectorWindowLoginButtonWidth-TextBounds.width)/2,SelectorWindowLoginButtonHeight-((SelectorWindowLoginButtonHeight-TextBounds.ascent)/2),"Login",sizeof("Login"));
    XFlush(dsp);
    Delay(ButtonPressDelayTime);
    Display_Selector_Window_Login_Button();
}

void Display_Login_Window(void)
{
    if (!LoginWindow)
    {
        font = XLoadQueryFont(dsp, "12x24");
        s = DefaultScreen(dsp);
        values.foreground = XBlackPixel(dsp, s);
        values.font = font->fid;
        valuemask = 0;
        valuemask |= GCForeground | GCFont;
        SelectorWindow = XCreateSimpleWindow(dsp, MainWindow,LoginWindowXLoc,LoginWindowYLoc,LoginWindowWidth,LoginWindowHeight,0,ColorWhite,BackgroundColor);
        XSelectInput(dsp, LoginWindow, StructureNotifyMask);
        XMapWindow(dsp, LoginWindow);
        gcLoginWindow = XCreateGC(dsp, LoginWindow, 0, NULL);
    }
    XRaiseWindow(dsp,LoginWindow);
    XClearWindow(dsp,LoginWindow);
    XSetForeground(dsp,gcLoginWindow,ButtonBorderColorA);
    XDrawLine(dsp,LoginWindow,gcLoginWindow,0,0,LoginWindowWidth-1,0);
    XDrawLine(dsp,LoginWindow,gcLoginWindow,0,0,0,LoginWindowHeight-1);
    XSetForeground(dsp,gcLoginWindow,ButtonBorderColorB);
    XDrawLine(dsp,LoginWindow,gcLoginWindow,0,LoginWindowHeight-1,LoginWindowWidth-1,LoginWindowHeight-1);
    XDrawLine(dsp,LoginWindow,gcLoginWindow,LoginWindowWidth-1,0,LoginWindowWidth-1,LoginWindowHeight-1);
    XSetForeground(dsp,gcLoginWindow,TextBoxColor);
    XFlush(dsp);
}
